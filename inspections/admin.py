# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from inspections.models import InspeccionOcular,FormInspeccion


admin.site.register(InspeccionOcular)
admin.site.register(FormInspeccion)


