from django.shortcuts import render
from django_tables2 import SingleTableView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.views.generic import DetailView
# Create your views here.


from .models import Solicitud
from .tables import SolicitudesTable 
from .forms import SolicitudForm


class SolicitudList(SingleTableView):
	model = Solicitud
	template_name = 'solicitudes/Solicitudes_list.html'
	table_class = SolicitudesTable

# class CreateSolicitud(CreateView):
# 	model

class SolicitudCreate(CreateView):
	model = Solicitud
	template_name = 'solicitudes/Solicitud_form.html'
	form_class = SolicitudForm
	success_url = reverse_lazy('solicitudes:solicitudes-list')
