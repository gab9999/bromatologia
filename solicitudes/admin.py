# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin



from solicitudes.models import Solicitud,Inspectores

admin.site.register(Solicitud)
admin.site.register(Inspectores)
