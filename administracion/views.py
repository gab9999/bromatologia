from django.shortcuts import render
from django.views.generic import TemplateView
# Create your views here.

class AdminIndex(TemplateView):
	template_name = 'administracion/index.html'