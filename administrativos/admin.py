from django.contrib import admin

# Register your models here.

from administrativos.models import Rubros,Comercios

admin.site.register(Rubros)
admin.site.register(Comercios)
