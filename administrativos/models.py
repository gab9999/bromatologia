from django.db import models

class Rubros(models.Model):
	cod_rubro = models.IntegerField('Codigo del Rubro')
	nomb_rubro = models.CharField('Nombre del Rubro', max_length=150)

	def __str__(self):
		return self.nomb_rubro

class Comercios(models.Model):

	ALTA = 'SI'
	BAJA = 'NO'

	ACTIVO =(
		(ALTA,'Si'),
		(BAJA,'No')
		)

	nomb_comercio = models.CharField('Nombre Comercio', max_length=150)
	propietario = models.CharField('Nombre Propietario', max_length=150)
	dni = models.IntegerField('D.N.I')
	domicilio = models.CharField('Domicilio Local', max_length=150)
	numeracion = models.IntegerField('Numeracion')
	fecha_soli = models.DateField('Fecha de Solicitud')
	Activo = models.CharField('Activo', max_length=2, choices=ACTIVO)
	numero_expte = models.CharField('Numero de Expediente', max_length=150)
	rubro = models.ManyToManyField(Rubros, blank=True)

	def __str__(self):
		return '{} {}'.format(self.nomb_comercio,self.propietario)