# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comercios',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nomb_comercio', models.CharField(max_length=150, verbose_name=b'Nombre Comercio')),
                ('propietario', models.CharField(max_length=150, verbose_name=b'Nombre Propietario')),
                ('dni', models.IntegerField(max_length=50, verbose_name=b'D.N.I')),
                ('domicilio', models.CharField(max_length=150, verbose_name=b'Domicilio Local')),
                ('numeracion', models.IntegerField(max_length=50, verbose_name=b'Numeracion')),
                ('fecha_soli', models.DateField(verbose_name=b'Fecha de Solicitud')),
                ('Activo', models.CharField(max_length=2, verbose_name=b'Activo', choices=[(b'SI', b'Si'), (b'NO', b'No')])),
                ('numero_expte', models.CharField(max_length=150, verbose_name=b'Numero de Expediente')),
            ],
        ),
        migrations.CreateModel(
            name='Rubros',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cod_rubro', models.IntegerField(max_length=50, verbose_name=b'Codigo del Rubro')),
                ('nomb_rubro', models.CharField(max_length=150, verbose_name=b'Nombre del Rubro')),
            ],
        ),
        migrations.AddField(
            model_name='comercios',
            name='rubro',
            field=models.ManyToManyField(to='administrativos.Rubros', blank=True),
        ),
    ]
