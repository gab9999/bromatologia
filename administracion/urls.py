from django.conf.urls import url, include

from .views import AdminIndex

urlpatterns = [

	url(r'^$', AdminIndex.as_view(), name='admin-index'),
	url(r'^inspections/', include('inspections.urls', namespace='inspections')),
	url(r'^comercios/', include('administrativos.urls', namespace='administrativos')),
	url(r'^solicitudes/', include('solicitudes.urls', namespace='solicitudes')),
	#url(r'^inspections/', include('inspections.urls', namespace='inspections')),
	#url(r'^inspections/', include('inspections.urls', namespace='inspections')),
	
]