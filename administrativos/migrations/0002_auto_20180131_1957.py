# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('administrativos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comercios',
            name='dni',
            field=models.IntegerField(verbose_name=b'D.N.I'),
        ),
        migrations.AlterField(
            model_name='comercios',
            name='numeracion',
            field=models.IntegerField(verbose_name=b'Numeracion'),
        ),
        migrations.AlterField(
            model_name='rubros',
            name='cod_rubro',
            field=models.IntegerField(verbose_name=b'Codigo del Rubro'),
        ),
    ]
