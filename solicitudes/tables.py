import django_tables2 as tables

from .models import Solicitud


class SolicitudesTable(tables.Table):

    controles = tables.TemplateColumn(
        template_name='solicitudes/list_controls.html',
        orderable=False
    )

    class Meta:
        model = Solicitud
        attrs = {
            "id": "solicitudes",
            "class": "table table-bordered table-hover"
        }

        table_pagination = {
            'per_page': 10
        }
