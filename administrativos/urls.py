from django.conf.urls import url
from . import views

#esto es una lista
urlpatterns = [
	
	url(
		r'^$',
		views.ComercioList.as_view(),
		name='comercios-list'

	),

	url(
		r'^detalle/(?P<pk>\d+)/$',
		views.ComercioDetail.as_view(),
		name='comercios-detail'

	),

	url(
		r'^alta/$',
		views.ComercioCreate.as_view(),
		name='comercios-create'

	),

	url(
		r'^modificar/(?P<pk>\d+)/$',
		views.ComercioUpdate.as_view(),
		name = 'comercios-update'
	),

	url(
		r'^eliminar/(?P<pk>\d+)/$',
		views.ComercioDelete.as_view(),
		name = 'comercios-delete'
	),
]
