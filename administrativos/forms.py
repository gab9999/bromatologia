from django import forms
from django.forms import inlineformset_factory

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Button, Div, Fieldset
from crispy_forms.bootstrap import FormActions, PrependedText

from administrativos.models import Comercios,Rubros



class AdministrativoForm(forms.ModelForm):

	class Meta:
		model = Comercios

		fields = (
			'nomb_comercio',
			'propietario',
			'dni',
			'domicilio',
			'numeracion',
			'fecha_soli',
			'Activo',
			'numero_expte',
			'rubro',
		)
	def __init__(self, *args, **kwargs):
		super(AdministrativoForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_tag = False

		propietario__nomb_comercio = Div(
			Div('propietario', css_class='col-md-6'),
			Div('nomb_comercio', css_class='col-md-6'),
			css_class='row'
		)

		dni__domicilio__numeracion = Div(
			Div('dni',css_class='col-md-4'),
			Div('domicilio',css_class='col-md-4'),
			Div('numeracion',css_class='col-md-4'),
			css_class='row'
		)

		fecha_soli__Activo = Div(
			Div('fecha_soli',css_class='col-md-6'),
	
			
			Div('Activo',css_class='col-md-6'),
			css_class='row'
		)

		numero_expte__rubro = Div(
			Div('numero_expte', css_class='col-md-6'),
			Div('rubro', css_class='col-md-6'),
			css_class='row'
		)

		self.helper.layout = Layout(
			propietario__nomb_comercio,
			dni__domicilio__numeracion,
			fecha_soli__Activo,
			numero_expte__rubro,

		)
		self.helper.layout.append(
			FormActions(
				Button('cancel', 'Cancelar', css_class='btn-danger', onclick="window.history.back()"),
				Submit('save','Guardar'),
			)

		)