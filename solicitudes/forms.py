from django import forms
from django.forms import inlineformset_factory

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Button, Div, Fieldset
from crispy_forms.bootstrap import FormActions, PrependedText

from solicitudes.models import Solicitud



class SolicitudForm(forms.ModelForm):

	class Meta:
		model = Solicitud

		fields = (
			'fecha_solicitud',
			'comercios',
			'inspector',
			'num_expte',
			
		)
	def __init__(self, *args, **kwargs):
		super(SolicitudForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper()
		self.helper.form_tag = False

		fecha_solicitud__num_expte = Div(
			Div('fecha_solicitud', css_class='col-md-6'),
			Div('num_expte', css_class='col-md-6'),
			css_class='row'
		)

		comercios__inspector = Div(
			Div('comercios',css_class='col-md-6'),
			Div('inspector',css_class='col-md-6'),
			css_class='row'
		)

		

		self.helper.layout = Layout(
			fecha_solicitud__num_expte,
			comercios__inspector,

		)
		self.helper.layout.append(
			FormActions(
				Button('cancel', 'Cancelar', css_class='btn-danger', onclick="window.history.back()"),
				Submit('save','Guardar'),
			)

		)