from django.shortcuts import render
from django_tables2 import SingleTableView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.views.generic import DetailView
# Create your views here.


from .models import Comercios,Rubros
from .tables import ComerciosTable 
from .forms import AdministrativoForm


class ComercioList(SingleTableView):
	model = Comercios
	template_name = 'administrativos/Comercios_list.html'
	table_class = ComerciosTable

	# def get_queryset(self):
	# 	query = super(ComercioList,self).get_queryset
	# 	return query.filter(Activo=True)

class ComercioCreate(CreateView):
	model = Comercios
	template_name = 'administrativos/Comercios_form.html'
	form_class = AdministrativoForm
	success_url = reverse_lazy('administrativos:comercios-list')

class ComercioDetail(DetailView):
	model = Comercios
	template_name = 'administrativos/Comercios_detail.html'
	form_class = AdministrativoForm


class ComercioUpdate(UpdateView):
	model = Comercios
	template_name = 'administrativos/Comercios_form.html'
	form_class = AdministrativoForm
	success_url = reverse_lazy('administrativos:comercios-list')


class ComercioDelete(DeleteView):
	model = Comercios
	template_name = 'administrativos/Comercios_delete.html'
	success_url = reverse_lazy('administrativos:comercios-list')





	# def get_context_data(self, *args, **kwargs):
	# 	context = super(ComercioDetail, self).get_context_data(*args, **kwargs)
	# 	context['objetos'] = Rubros.objects.all()
	# 	return context
	
	

	# def get_context_data(self, *args, **kwargs):
	# 	context = super(ComercioDetail, self).get_context_data(*args, **kwargs)
	# 	context['objeto2'] = Rubros.objects.get(comercios=self.object)
	# 	return context
