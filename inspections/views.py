# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django_tables2 import SingleTableView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.views.generic import DetailView

from .models import InspeccionOcular
from .tables import InspeccionTable
# Create your views here.



class InspeccionList(SingleTableView):
	model = InspeccionOcular
	template_name = 'inspecciones/Inspecciones_list.html'
	table_class = InspeccionTable