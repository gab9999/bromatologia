from django.conf.urls import url
from inspections.views import InspeccionList

urlpatterns = [
	
	url(
		r'^listar',
		InspeccionList.as_view(),
		name='inspecciones-list'

	),

]