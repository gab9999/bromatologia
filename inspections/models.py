# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from solicitudes.models import Solicitud,Inspectores
from administrativos.models import Comercios
from multiselectfield import MultiSelectField

from django.utils import timezone
# Create your models here.

class InspeccionOcular(models.Model):

	

	solicitud = models.ForeignKey(Solicitud, null=True, blank=True ,on_delete=models.CASCADE)
	comercios = models.ForeignKey(Comercios, null=True, blank=True ,on_delete=models.CASCADE)
	fecha_ins = models.DateField(default=timezone.now)
	
	


	def __str__(self):
		return '{} {} {}'.format(self.id,self.solicitud, self.comercios)


class FormInspeccion(models.Model):

	MATERIAL_TECHO_CHOICES = (
		(1,'HA Losa'),
		(2,'Revocado Pintado'),
		(3,'Chapa de Zinc')
	)
	CIELO_RAZO_CHOICES = (
		(1,'Expandido'),
		(2,'Suspendido')
	)
	TIPO_CHOICES = (
		(1,'Durlock'),
		(2,'Madera'),
		(3,'Telgopor'),
		(4,'Extructura Metalica')
	)
	MUROS_CHOICES = (
		('Si','Si'),
		('No','No')
	)
	EXTRUCTURAMETALICA_CHOICES = (
		('Si','Si'),
		('No','No')
	)
	MADERA_CHOICES= (
		('Si','Si'),
		('No','No')
	)
	PINTADO_CHOICES = (
		('Si','Si'),
		('No','No')
	)
	TIPOPINTADO_CHOICES = (
		(1,'Latex'),
		(2,'Sintetico'),
		(3,'Agua'),
		(4,'Exposi')
	)
	REVESTIMIENTO_CHOICES = (
		('Si','Si'),
		('No','No')
	)
	TIPOREVESTIMIENTO_CHOICES = (
		(1,'Azulejo'),
		(2,'Ceramico'),
		(1,'Otros')
	)
	ESTADOS_CHOICES = (
		('Aprobado','Aprobado'),
		('Preaprobado','Preaprobado'),
		('Desaprobado','Desaprobado')
	)

	material = MultiSelectField('Material', choices= MATERIAL_TECHO_CHOICES, max_choices=3, max_length=3)
	cielo_razo = MultiSelectField('Cielo Razo', choices= CIELO_RAZO_CHOICES, max_choices=2, max_length=2)
	tipo_techo = MultiSelectField('Tipo De Techo', choices= TIPO_CHOICES, max_choices=4, max_length=4)
	muros_rev = models.CharField('Muros Revocados', choices= MUROS_CHOICES, max_length=2, null =True)
	ext_met = models.CharField('Extructura Metalica', choices= EXTRUCTURAMETALICA_CHOICES, max_length=2, null=True)
	madera = models.CharField('Madera', choices= MADERA_CHOICES, max_length=2, null=True)
	pintado = models.CharField('Pintado',choices= PINTADO_CHOICES ,max_length=2,null = True)
	tipo_pint = MultiSelectField('Tipo', choices= TIPOPINTADO_CHOICES, null=True)
	revestimiento = models.CharField('Revestimiento',choices= REVESTIMIENTO_CHOICES ,max_length=2,null = True)
	mts = models.IntegerField('Mts',null=True)
	tipo_rev = MultiSelectField('Tipo', choices= TIPOREVESTIMIENTO_CHOICES, null = True)
	tipo_rev_otros = models.CharField('Otros.',max_length=150,null = True)
	estado = models.CharField('Estado', max_length=150, choices=ESTADOS_CHOICES, null=True)
	observaciones = models.CharField('Observaciones', max_length=150 , null=True)
