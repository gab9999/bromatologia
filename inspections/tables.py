import django_tables2 as tables

from .models import InspeccionOcular


class InspeccionTable(tables.Table):

    controles = tables.TemplateColumn(
        template_name='inspecciones/list_controls.html',
        orderable=False
    )

    class Meta:
        model = InspeccionOcular
        attrs = {
            "id": "inspections",
            "class": "table table-bordered table-hover"
        }

        table_pagination = {
            'per_page': 10
        }
