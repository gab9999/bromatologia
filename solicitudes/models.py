# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from administrativos.models import Comercios

class Inspectores(models.Model):
	nombre = models.CharField('Nombre', max_length=100)
	apellido = models.CharField('Apellido', max_length=100)
	dni = models.CharField('DNI', max_length=8)
	tel = models.IntegerField('Telefono')
	estado = models.BooleanField('Disponibilidad', default=True)

	def __str__(self):
		return '{} {}'.format(self.nombre, self.apellido)


class Solicitud(models.Model):
	solicitud_id = models.AutoField(primary_key=True)
	fecha_solicitud = models.DateField('Fecha de Solicitud')
	comercios = models.ForeignKey(Comercios, null=True, blank=True ,on_delete=models.CASCADE)
	inspector = models.ForeignKey(Inspectores, null=True, blank=True, on_delete=models.CASCADE)
	num_expte = models.PositiveIntegerField('Numero de Expediente' , null=True)


	def __str__(self):
		return '{} {} {}'.format(self.solicitud_id,self.fecha_solicitud,self.inspector)
		