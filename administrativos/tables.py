import django_tables2 as tables

from .models import Comercios


class ComerciosTable(tables.Table):

    controles = tables.TemplateColumn(
        template_name='administrativos/list_controls.html',
        orderable=False
    )

    class Meta:
        model = Comercios
        attrs = {
            "id": "administrativos",
            "class": "table table-bordered table-hover"
        }

        table_pagination = {
            'per_page': 10
        }
